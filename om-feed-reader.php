<?php
/*
Plugin Name: OM Caleb RSS Feed Reader
Description: Plugin to view an rss feed from Caleb on your page or post.
	[om-gc-feed-reader time=11-2006 countryId=UG categoryID=25 text="Surfing" length=10
	start=10 families=1/0 groups=1/0 married=1/0 age=16 pastDays=-30 plain=0/1]
or  [om-jobs-feed-reader category=1 country=IE region=17 freeText="Programmer" length=10 plain=0/1]
	Simply use the shortcode and the RSS feed will be shown in HTML to your visitor
	All options are optional, so simply [om-jobs-feed-reader] will work.
Version: 0.0.6
Text Domain: om_feed_reader
Author: OMNIvision
Author URI: http://www.omnivision.org.uk
License: GPL2
*/

define('OM_APP_URL', 'http://app.om.org/rss/');
define('OM_APP_RESOURCE_URL', OM_APP_URL.'resource.rss');
define('OM_APP_RESOURCES_URL', OM_APP_URL.'resources.rss');
define('OM_APP_SHORTTERMS_URL', OM_APP_URL.'challenge.rss');
define('OM_APP_JOBS_URL', OM_APP_URL.'opportunities.rss');
define('OM_APP_CONTACTS_URL', OM_APP_URL.'contacts.rss');

define('OM_APP_DEFAULT_FROMCOUNTRY', 'GB');


if (is_admin()) {
	include_once('om-feed-reader-options.php');
	include_once('functions.php');
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/* *************************************************************************
 *
 * Useful bits and pieces.  Could be factored out into external libs.
 * There's no 'Wordpress' reason for these to be here...
 *
 * ************************************************************************* */

/*
 * HTag - dead simple PHP function for helping make HTML tags,
 * without having to do all the escaping (etc) yourself.
 *
 >>> HTag('a',array('href'=>'www.google.com'),'Google!')
 <a href="www.google.com">Google!</a>

 >>> HTag('input',array('checked'=>true, 'name'=>'banana'),'/')
 <input checked name="banana" />

 >>> HTag('div,'array('"stupid',=>'name"'))
 <div stupid="name&quot;"></div>

 *
 */
function HTag($name, $parts=array(), $content='')
{
	$html = '<' . $name . ' ';
	foreach ($parts as $key => $val) {
		if ($val === true) {
			$html .= preg_replace('/[\s\'">=]/','', $key);
		} elseif ($val === false) {
			// do nothing!
		} else {
			$html .= preg_replace('/[\s\'">=]/','', $key);
			$html .= '="' . htmlspecialchars($val, ENT_COMPAT, 'UTF-8') . '" ';
		}
	}

	if ($content == '/') {
		$html .= '/>';
	} else {
		$html .= '>' . $content . '</' . $name . '>';
	}
	return $html;
}

/**
 * issetandtrue
 * @param mixed $thing to test
 * @param mixed $default to return if $thing is not set, or false.
 *
 * @returns $thing if it's set, and evaluates to true (<> 0 if numeric, not '' if string), else $default
 */
function issetandtrue(&$thing, $default=False)
{
	return ((isset($thing) && $thing) ? $thing : $default);
}


/* unused??? */
function OmFeedfirstWords($string, $words=100)
{
	$wo = explode(" ", $string);
	$c=0;
	$return='';
	foreach ($wo as $piece){
		$c++;
		if ($words>$c) {
			$return .= " ".$piece;
		}
	}
	return $return;
}

// check for cached text from a url, otherwise fetch it
// then convert it to an xml object
function get_caleb_xml_object($url)
{
	$key = md5($url);
	if (false === ($xml = get_transient('caleb_rss_'.$key))) {
		$xml = get_caleb_feed($url);
		if ($xml) {
			$options = wp_parse_args(get_option('om-reader_options'));
			set_transient('caleb_rss_'.$key, $xml, $options['cache_timeout'] * HOUR_IN_SECONDS); // 12 hours
		}
	}
	return $xml ? @simplexml_load_string($xml) : false;
}

/**
 * om_from_country - returns visitor's country of origin.  Either as specified by post/site author,
 * or falling back to 'VisitorCountry' GeoIP, or if that's not available, then the OM_APP_DEFAULT_FROMCOUNTRY.
 *
 * @param string &$preferred If this value is set, use it.  By reference, so you can use om_from_country($x['y'])
 *
 * @returns string
 */
function om_from_country(&$preferred)
{
	global $VisitorCountry;

	if (issetandtrue($preferred)) {
		return $preferred;
	} elseif (isset($VisitorCountry) && $VisitorCountry->GetCode() !== '') {
		return $VisitorCountry->GetCode();
	} else {
		return OM_APP_DEFAULT_FROMCOUNTRY;
	}
}


/**
 * fix an array sent to us from a shorttag, for instance, setting the keys to the correct
 * names.
 */
function replace_array_keys(&$input_array, $replace_map)
{
	foreach ($replace_map as $key => $new_key) {
		if (isset($input_array[$key])) {
			$input_array[$new_key] = $input_array[$key];
			unset ($input_array[$key]);
		}
	}
}

/**
* Actually render the output(s) for visitors:
*
* @param string $style: (gc|jobs|resources) (which template to use)
* @param string $url: (XML url from caleb to download)
*/

function om_rss_get_and_render($style, $url, $limit=50, $atts=array())
{

	load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/' );
	
	$global_settings = wp_parse_args(get_option('om-reader_options'));

	$url = html_entity_decode($url);

	$xml = get_caleb_xml_object($url);

	$i=0;
	$lastcountry = "";
	if (!count($xml->channel->item)) {
		return false;
	}

	switch($style) {
	case "gc":
		include('layouts/shortterms/index.php');
		break;
	case "jobs":
		include('layouts/jobs/index.php');
		break;
	case "resources":
		include('layouts/resources/index.php');
		break;
	}

	return $return . '<!-- ' . $url . '-->';
}


function om_listwidget_item($config, $item)
{
	$return = '<li>';

	if ($config['max_title_length']) {
		$title = wp_trim_words($item['title'], $config['max_title_length']);
	} else {
		$title = $item['title'];
	}

	$return .= '<h4>';

	$return .= HTag('a', array('href' => $item['link'],
							   'title' => $title,
							   'target' => '_blank',
							   'class' => 'om-feed-link'),
					$title);

	$return .= '</h4>';


	if ($config['show_desc']) {
		$return .= '<p>';
		if ($config['max_description_length']) {
			$return .= wp_trim_words($item['description'], $config['max_description_length']);
		} else {
			$return .= substr($item['description'], 0, $config['max_description_length']) . '&hellip;';
		}
		$return .= "</p>";
	}

	$return .= '</li>';
	return $return;
}




/**
 * The Widget
 */
class OMGCWidget extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct(false, $name = 'OM Short Term Missions');
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		//get arguments
		global $q_config;

		load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/languages' );

		extract( $args );

		//set title var
		$title = 'OM Short Term Missions Widget';

		//get values from config
		$title = (isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : $title);
		$limit = issetandtrue($instance['limit'], 5);
		$shorten = issetandtrue($instance['shorten'], 0);
		$hide_description = issetandtrue($instance['hide_description'], 0);

		// config for om_listwidget_item
		$config = array(
			'max_title_length' => ($shorten ? 40 : 400),  // TODO: remove magic numbers...
			'show_desc' => (!$hide_description),
			'max_description_length' => $instance['max_description_length']
			);

		// build the caleb query:

		$qargs = array(
			'fromCountryId' => om_from_country($instance['from_country']),
			'langCode'	  => issetandtrue($q_config['language'], (WPLANG ?: 'en')),
			'time'		  => issetandtrue($instance['time'], NULL),
			'countryId'	 => issetandtrue($instance['countryId'], NULL),
			'categoryId'	=> issetandtrue($instance['categoryId'], NULL),
			'text'		  => issetandtrue($instance['text'], NULL),
			'pastDays'	  => issetandtrue($instance['pastDays'], NULL)
			);

		// Convert 'normal' countryIds into OM style...
		if (strlen($qargs['countryId']) == 2) {
			$qargs['countryId'] .= 'Q';
		}

		// TODO: support multiple countries, Comma Separated

		$url = OM_APP_SHORTTERMS_URL . '?' . http_build_query($qargs);

		$xml = get_caleb_xml_object($url);

		$return .= '<h3>' . $title . '</h3>';
		$return .= "<ul class=\"om_listwidget om-gc-list\">";

		$i = 1;
		foreach($xml->channel->item as $item) {
			$om = $item->children('om', TRUE);

			/*
			 * Attempt to narrow the results. In theory, doing this here will help caching.
			 */

			if (issetandtrue($instance['families']) && $om->familiesWelcome != "t") {
				continue;
			}
			if (issetandtrue($instance['groups']) && $om->groupsWelcome != "t") {
				continue;
			}
			if (issetandtrue($instance['married']) && $om->marriedWelcome != "t") {
				continue;
			}

            if (isset($om->availabilityStatus) && 
                ($om->availabilityStatus == 'FULL' || $om->availabilityStatus == 'CANCELLED')) {
                continue;
            }

			if (issetandtrue($instance['age'])) {
				if ($om->minAge!='any' && $om->minAge > $instance['age']) {
					continue;
				}
				if ($om->maxAge!='any' && $om->maxAge < $instance['age']) {
					continue;
				}
			}

			if ($i++ > $limit) {
				break;
			}


			$return .= om_listwidget_item($config, array(
								'title' => $om->eventName,
								'link' => $item->link,
								'description' => $om->description
								));

		}

		$return .= '</ul><br />' . HTag('a', array('href' => $xml->channel->link,
												   'class' => 'om-gc-feed-url'),
										__("More short-term global opportunities", "om_feed_reader"));


		/* Display the widget */

		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		echo $return;
		echo $after_widget;

	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['time'] = strip_tags($new_instance['time']);
		$instance['categoryId'] = strip_tags($new_instance['categoryId']);
		$instance['countryId'] = strip_tags($new_instance['countryId']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['families'] = strip_tags($new_instance['families']);
		$instance['groups'] = strip_tags($new_instance['groups']);
		$instance['married'] = strip_tags($new_instance['married']);
		$instance['age'] = strip_tags($new_instance['age']);
		$instance['pastDays'] = strip_tags($new_instance['pastDays']);
		$instance['limit'] = (int)($new_instance['limit']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['shorten'] = (int)($new_instance['shorten']);
		$instance['max_description_length'] = (int)($new_instance['max_description_length']);
		$instance['hide_description'] = (int)($new_instance['hide_description']);
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		/*in8n*/
		load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/languages' );

		/* load values */

		$title =			  esc_attr(issetandtrue($instance['title'], ''));
		$limit =		(int) esc_attr(issetandtrue($instance['limit'], '5'));
		$shorten =  (boolean) esc_attr(issetandtrue($instance['shorten'], '0'));
		$families = (boolean) esc_attr(issetandtrue($instance['families'], '0'));
		$groups =   (boolean) esc_attr(issetandtrue($instance['groups'], '0'));
		$married =  (boolean) esc_attr(issetandtrue($instance['married'], '0'));
		$time =			   esc_attr(issetandtrue($instance['time'], ''));
		$countryId =		  esc_attr(issetandtrue($instance['countryId'], ''));
		$categoryId =		 esc_attr(issetandtrue($instance['categoryId'], ''));
		$text =			   esc_attr(issetandtrue($instance['text'], ''));
		$hide_description =	   (boolean) esc_attr(issetandtrue($instance['hide_description'], ''));
		$max_description_length =	 (int) esc_attr(issetandtrue($instance['max_description_length'], '60'));

		echo '<p>';

		// TITLE:

		echo '<label>', __('Title:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('title'),
								 'type' => 'text',
								 'class' => 'widefat',
								 'value' => $title,
								 'name' => $this->get_field_name('title')), '/');
		echo '</label><br/>';

		// Time:

		echo '<label>', __('Time:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('time'),
								 'type' => 'text',
								 'class' => 'widefat',
								 'value' => $time,
								 'name' => $this->get_field_name('time')), '/');
		echo '</label><br/>';

		// CountryID:

		echo '<label>', __('Country Id:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('countryId'),
								 'type' => 'text',
								 'placeholder' => 'GBQ for instance...',
								 'value' => $countryId,
								 'name' => $this->get_field_name('countryId')), '/');
		echo '</label><br/>';

		// CategoryId:

		echo '<label>', __('Category Id:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('categoryId'),
								 'type' => 'text',
								 'value' => $categoryId,
								 'name' => $this->get_field_name('categoryId')), '/');
		echo '</label><br/>';

		// Text Search:

		echo '<label>', __('Text Search', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('text'),
								 'type' => 'text',
								 'value' => $text,
								 'name' => $this->get_field_name('text')), '/');
		echo '</label><br/>';

		// For Families:

		echo '<label>', __('Must accept families?', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('families'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('families'),
								 'checked' => ($families == 1)), '/');
		echo '</label><br/>';

		// For Groups:

		echo '<label>', __('Must accept groups?', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('groups'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('groups'),
								 'checked' => ($groups == 1)), '/');
		echo '</label><br/>';

		// For Married:

		echo '<label>', __('Must accept married couples?', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('married'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('married'),
								 'checked' => ($married == 1)), '/');
		echo '</label><br/>';


		// Number of messages limit:

		echo '<label>', __('Maximum number to show:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('limit'),
								 'type' => 'number',
								 'value' => $limit,
								 'name' => $this->get_field_name('limit')), '/');
		echo '</label><br/>';

		// Shorten Link:

		echo '<label>', __('Shorten link', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('shorten'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('shorten'),
								 'checked' => ($shorten == 1)), '/');
		echo '</label><br/>';

		// Hide description

		echo '<label>', __('Hide Description', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('hide_description'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('hide_description'),
								 'checked' => ($hide_description == 1)), '/');
		echo '</label><br/>';

		echo '<label>', __('Maximum Description Length:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('max_description_length'),
								 'type' => 'number',
								 'value' => $max_description_length,
								 'name' => $this->get_field_name('max_description_length')), '/');
		echo '</label><br/>';



		echo '</p>';

	}

}

add_action('widgets_init', create_function('', 'return register_widget("OMGCWidget");'));

/**
 * Jobs Widget
 */
class OMJobsWidget extends WP_Widget
{
	var $title = 'OM Jobs Widget';
	var $limit = 5;
	var $shorten = 0;
	var $hide_description = 0;

	/** constructor */
	function __construct()
	{
		parent::__construct(false, $name = 'OM Jobs');
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{

		load_plugin_textdomain('om_feed_reader', false, basename( dirname( __FILE__ ) ) . '/languages' );

		extract( $args );

		//get defaults
		$title = (isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : $this->title);
		$limit = issetandtrue($instance['limit'], 5);
		$shorten = issetandtrue($instance['shorten'], 0);
		$hide_description = (isset($instance['hide_description']) ?: $this->hide_description);

		// config for om_listwidget_item
		$config = array(
			'max_title_length' => ($shorten ? 40 : 400),  // TODO: remove magic numbers...
			'show_desc' => True, //(!$hide_description),
			'max_description_length' => 100 //$instance['max_description_length']
			);

		// Construct URL
		$qargs = array(
			'country'  => issetandtrue($instance['countryId'], NULL),
			'categoryId' => issetandtrue($instance['categoryId'], NULL),
			'freeText'   => issetandtrue($instance['text'], NULL)
			);

		// Get data from Caleb
		$xml = get_caleb_xml_object(OM_APP_JOBS_URL . '?' . http_build_query($qargs));


		//////////////
		// Actually build the list widget HTML:

		$return = '<h3>' . $title . '</h3>';
		$return .= '<ul class="om_listwidget om-jobs-list">';

		$i = 1;
		foreach($xml->channel->item as $item) {
			if ($i++ > $limit) {
				break;
			}

			$om = $item->children('om', TRUE);

			$return .= om_listwidget_item($config, array(
								'title' => $om->title,
								'link' => $item->link,
								'description' => (string) $om->description
								));

		}

		$return .= '</ul><br /><a href="' . $xml->channel->link . '" class="om-jobs-feed-url">'
				. __("More long-term global opportunities&hellip;", "om_feed_reader") . '</a>';

		/* display widget */
		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		echo $return;
		echo $after_widget;

	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['categoryId'] = strip_tags($new_instance['categoryId']);
		$instance['countryId'] = strip_tags($new_instance['countryId']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['limit'] = (int)($new_instance['limit']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['shorten'] = (int)($new_instance['shorten']);
		$instance['hide_description'] = (int)($new_instance['hide_description']);
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = (isset($instance['title']) ? esc_attr($instance['title']) : '');
		$limit = (isset($instance['limit']) ? esc_attr($instance['limit']) : 5);
		$shorten = (isset($instance['shorten']) ? esc_attr($instance['shorten']) : 0);
		$countryId = (isset($instance['countryId']) ? esc_attr($instance['countryId']) : '');
		$categoryId = (isset($instance['categoryId']) ? esc_attr($instance['categoryId']) : '');
		$text = (isset($instance['text']) ? esc_attr($instance['text']) : '');
		$hide_description = (isset($instance['hide_description']) ? esc_attr($instance['hide_description']) : 0);

		echo '<p>';

		// TITLE:

		echo '<label>', __('Title:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('title'),
								 'type' => 'text',
								 'class' => 'widefat',
								 'value' => $title,
								 'name' => $this->get_field_name('title')), '/');
		echo '</label><br/>';

		// CountryID:

		echo '<label>', __('Country Id:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('countryId'),
								 'type' => 'text',
								 'value' => $countryId,
								 'name' => $this->get_field_name('countryId')), '/');
		echo '</label><br/>';

		// CategoryId:

		echo '<label>', __('Category Id:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('categoryId'),
								 'type' => 'text',
								 'value' => $categoryId,
								 'name' => $this->get_field_name('categoryId')), '/');
		echo '</label><br/>';

		// Text Search:

		echo '<label>', __('Text Search', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('text'),
								 'type' => 'text',
								 'value' => $text,
								 'name' => $this->get_field_name('text')), '/');
		echo '</label><br/>';

		// Number of messages limit:

		echo '<label>', __('No# messages:', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('limit'),
								 'type' => 'number',
								 'value' => $limit,
								 'name' => $this->get_field_name('limit')), '/');
		echo '</label><br/>';

		// Shorten Link:

		echo '<label>', __('Shorten link', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('shorten'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('shorten'),
								 'checked' => ($shorten == 1)), '/');
		echo '</label><br/>';

		// Hide description

		echo '<label>', __('Hide Description', 'om_feed_reader'), ' &nbsp; ';

		echo HTag('input', array('id' => $this->get_field_id('hide_description'),
								 'type' => 'checkbox',
								 'value' => 1,
								 'name' => $this->get_field_name('hide_description'),
								 'checked' => ($hide_description == 1)), '/');
		echo '</label><br/>';

		echo '</p>';

	}

}

add_action('widgets_init', create_function('', 'return register_widget("OMJobsWidget");'));

/*
* short tag functions for short term and jobs
*
* enables:
* [om-gc-feed time=x countryId=ABC categoryId=99 text=ABC length=99 start=date
*			 families=0/1 groups=0/1 married=0/1 age=99 pastDays=99 plain=0/1]
* [om-jobs-feed country=ABC text=ABC length=99 plain=0/1]
*
*/
function om_gc_feed_shorttag($atts)
{
	global $q_config;
	$options = wp_parse_args(get_option('om-reader_options'));


	// TODO: fromCountryID can be specified more than once so we need to
	// account for this. So that OM Africa can run a site for all of the
	// African fields.
	$fromCountryID = om_from_country($options['from_country_id']);

	// Get the shortcode-specified attributes, & defaults:

	// Make shortcode tags case insensitive:
	replace_array_keys($atts, array(
		'countryid' => 'countryId',
		'categoryid' => 'categoryId',
		'fromcountryid' => 'fromCountryId'
		));

	// add in the defaults:
	$atts = shortcode_atts(array(
		'time'       => null,
		'countryId'  => null,
		'categoryId' => null,
		'text'       => null,
		'length'     => '10',
		'start'      => null,
		'families'   => null,
		'groups'     => null,
		'married'    => null,
		'age'        => null,
		'pastdays'   => null,
		'fromCountryId' => $fromCountryID,
		'groupby'    => 'country',
		'plain'      => '0',
		), $atts);

	// Set up the URL & parameters:

	$feed_url = OM_APP_SHORTTERMS_URL.'?';

	$langCode = $q_config['language'];
	$feed_url .= "langCode=" . $langCode;

	// expand out comma,separated,countries, and separate from the array
	$fromCountries = '&fromCountryId=' . implode('&fromCountryId=', explode(',', $atts['fromCountryId']));
	unset($atts['fromCountryId']);

	$feed_url .= '&' . http_build_query($atts);

	//$limit =		(int) esc_attr(issetandtrue($instance['limit'], '5'));

	// TODO: Should $limit come from elsewhere???
	// this is just to remove warnings.
	$limit = 50; // default

	if ($return = om_rss_get_and_render('gc', $feed_url . $fromCountries, $limit, $atts)) {
		return $return;
	} else { // If no results get the Default Country Results
		$return = om_rss_get_and_render('gc', $feed_url . '&fromCountryId=' . OM_APP_DEFAULT_FROMCOUNTRY, $limit, $atts);
		if ($return) {
			return $return;
		} else {
			return __('Sorry, no results', 'om_feed_reader');
		}
	}
}
add_shortcode( 'om-gc-feed-reader', 'om_gc_feed_shorttag'); //leave old for backwards compatibility.
add_shortcode( 'om_gc_feed_reader', 'om_gc_feed_shorttag');

function om_jobs_feed_shorttag($atts)
{
	$feed_url = OM_APP_JOBS_URL.'?';

	$atts = shortcode_atts(array(
		'country'	 => null,
		'category'	=> null,
		'region'	  => null,
		'freeText'	=> null,
		'length'	  => '1000',
		'groupby'	 => 'country',
		'plain'	   => 1,
		), $atts);

	$feed_url .= http_build_query($atts);

	$return = om_rss_get_and_render('jobs', $feed_url, $limit, $atts);
	if ($return) {
		return $return;
	} else {
		return __('Sorry, no results', 'om_feed_reader');
	}
}
add_shortcode( 'om-jobs-feed-reader', 'om_jobs_feed_shorttag'); //leave old for backwards compatibility.
add_shortcode( 'om_jobs_feed_reader', 'om_jobs_feed_shorttag');

/*
* Short tag function for displaying the local OM office's contact details
*/
function om_contact_info($country)
{
	global $om_local_field_gc_contact;
	global $om_local_field_contact;

	$options = wp_parse_args(get_option('om-reader_options'));

	if ($country === 'none') {
		$fromCountryID = om_from_country($options['from_country_id']);
	} else {
		$fromCountryID = $country;
	}

	/* Get the rss file. Yes, you just have to get ALL the contacts */
	$xml = get_caleb_xml_object(OM_APP_CONTACTS_URL);

	/* Do nothing if there's no items */
	if (!count($xml->channel->item)) {
		return false;
	}

	foreach($xml->channel->item as $item) {
		$om = $item->children('om', true);

		if ($om->countryId == $fromCountryID) {

			$field = array(
				'unitName' => $om->unitName,
				'tel' => $om->tel,
				'address' => $om->address,
				'email' => $om->email
			);

			/* Classify the contact as GC or General. */
			if ($om->type=="GC") {
				$om_local_field_gc_contact = $field;
			} else {
				$om_local_field_contact = $field;
			}
			unset($field);
		}
	}

	if (!isset($om_local_field_gc_contact['unitName']) && !isset($om_local_field_contact['unitName'])) {
		return false;
	} else {
		return true;
	}
}

function om_contact_display_shorttag($atts)
{
	global $om_local_field_gc_contact;
	global $om_local_field_contact;

	extract(shortcode_atts(array(
		'type'	=> 'gc',
		'country'	=> 'none',
		), $atts));

	if (!om_contact_info($country)) {
		return __("Sorry, no contact information for your local field", "om_feed_reader");
	}

	/* return the requested type of contact - fallback to the other type or null */
	switch ($type) {
		case 'general':
			if (isset($om_local_field_contact)) {
				$contact = $om_local_field_contact;
			} elseif (isset($om_local_field_gc_contact)) {
				$contact = $om_local_field_gc_contact;
			}
			break;
		case 'gc':
		default:
			if (isset($om_local_field_gc_contact)) {
				$contact = $om_local_field_gc_contact;
			} elseif (isset($om_local_field_contact)) {
				$contact = $om_local_field_contact;
			}
			break;
	}

	$scriptopen = '<SCRIPT LANGUAGE="JavaScript" type="text/javascript"> ';
	$scriptclose = ''
			." </SCRIPT>"
			."<NOSCRIPT>"
			."<em>".__("Email address protected by JavaScript. Activate javascript to see the email.", "om_feed_reader")."</em>"
			."</NOSCRIPT>\n";

	$emailregex = '(([a-zA-Z0-9\.]+)\@([a-zA-Z0-9\-]+\..+))';
	$emailreplace = $scriptopen."gen_mail_to_link('$1', '$2', 'Website Enquiry');".$scriptclose;
	$webregex = "%((https?://)|(www\.))(([a-z0-9\-]+\.?)+)/?(([a-zA-Z0-9\-]+/?)*)%i";
	$webreplace = '<a href="http://$1$4/$6">$1$4/$6</a>';

	$contact['email'] = preg_replace($emailregex, $emailreplace, $contact['email']);
	$contact['address'] = nl2br(preg_replace(array($webregex, $emailregex), array($webreplace, $emailreplace), $contact['address']));

	ob_start();
	include('layouts/contact/templates/default/item.html.php');
	$item = ob_get_contents();
	ob_end_clean();

	ob_start();
	include('layouts/contact/templates/default/page.html.php');
	$return = ob_get_contents();
	ob_end_clean();

	return $return;
}
add_shortcode('om-contact-display', 'om_contact_display_shorttag'); //leave old for backwards compatibility.
add_shortcode('om_contact_display', 'om_contact_display_shorttag');

/**
* function to add the general or short term email addresa as the recipient to CF7 forms
*/
function wpcf7_om_recipient (&$wpcf7)
{
	global $om_local_field_gc_contact;
	global $om_local_field_contact;

	// query the additional settings section of the form and if it has
	// contact_info: shortterm
	// OR
	// contact_info: general
	// we then change the appropriate recipient for the form.
	$contact_type = $wpcf7->additional_setting('contact_info', false);

	if (is_array($contact_type) && count($contact_type) > 0) {
		om_contact_info('none');

		switch ($contact_type) {
			case "shortterm":
				// this makes certain that the email will still get through no matter what
				$wpcf7->mail['recipient'] = isset($om_local_field_gc_contact['email']) ?: $om_local_field_contact['email'];
				break;
			case 'general': default:
				// this makes certain that the email will still get through no matter what
				$wpcf7->mail['recipient'] = isset($om_local_field_contact['email']) ?: $om_local_field_gc_contact['email'];
				break;
		}
	}
	$wpcf7_data->mail['body'] .= $wpcf7->additional_setting('contact_info');
	return $wpcf7;
}
if (class_exists('WPCF7_ContactForm')) { // && is_plugin_active('contact-form-7-modules/hidden.php')) {
	add_action("wpcf7_before_send_mail", "wpcf7_om_recipient");
}

/* short tag function for displaying a single resource */
function om_single_resource_shorttag($atts)
{
	$resource_id = urlencode($_GET['resourceID']);
	$url = OM_APP_RESOURCE_URL . "?id=" . $resource_id;
	$url = html_entity_decode($url);
	$xml = get_caleb_xml_object($url);

	$item = $xml->channel->item;
	$om = $item->children('om', true);

	/* display format varies with media type */
	if ($om->mediaTypeId == 0) {
		/* 0 = Image */
		return "<a href='" . $om->mediaUrl . "'><img src='" . $om->mediaUrl . "'></a>"
			   . "<h2>" . $item->title . "</h2>"
			   . "<p>" . $item->description . "</p>"
			   . "<span>Photo by " . $om->authorName . " - " . $om->creditDescription . "</span>"
			   . "<div style='clear:both;'></div>";
	} else {
		/* 4 = Article. */
		return "<h2>" . $item->title . "</h2>"
			   . "<a data-rokbox rel='rokbox' href='http://www.om.org/img/m" . $om->attachedPhotoId . ".jpg'"
			   . "   rel='rokbox'><img src='http://www.om.org/img/r".$om->attachedPhotoId.".jpg'"
			   . "   style='float:right; max-width:50% !important; min-width:300px;'></a>"
			   . "<p>" . $item->pubDate . "</p>"
			   . $om->full
			   . "<div style='clear:both;'></div>";
	}

	// TODO: also add 404 code?
	return __('Sorry, selected resource not found', 'om_feed_reader');
}
add_shortcode('om_single_resource', 'om_single_resource_shorttag');
add_shortcode('om-resource', 'om_single_resource_shorttag');

function om_resources_shorttag($atts)
{
	$feed_url = OM_APP_RESOURCES_URL."?";

	$atts = shortcode_atts(array(
		'mediatypeid'	  => null,
		'region'		   => null,
		'countryid'		=> null,
		'categoryid'	   => null,
		'text'			 => null,
		'length'		   => '10',
		'start'			=> null,
		'sort0'			=> null,
		'sort1'			=> null,
		'sort2'			=> null,
		'field'			=> null,
		'pin'			  => null,
		'includepublic'	=> null,
		'plain'			=> '1',
		), $atts);

	// make case insensitive for common fields
	replace_array_keys($atts, array(
			'mediatypeid' => 'mediaTypeId',
			'countryid' => 'countryId',
			'categoryid' => 'categoryId',
			'includepublic' => 'includePublic'
			));

	$feed_url .= http_build_query($atts);

	$return = om_rss_get_and_render('resources', $feed_url, $limit, $atts);

	if ($return) {
		return $return;
	} else {
		return __('Sorry, no results', 'om_feed_reader');
	}
}

add_shortcode('om-resources', 'om_resources_shorttag'); //leave old for backwards compatibility.
add_shortcode('om_resources', 'om_resources_shorttag');

function get_caleb_feed($url)
{
	$data = false;
	if ($url) {
		$response = wp_remote_get($url, array('timeout' => 15));
		if (!is_wp_error($response)) {
			$data = wp_remote_retrieve_body($response);
		}
	}
	return $data ? $data : false;
}

function set_om_feed_defaults()
{
	update_option('om-reader_options', array(
		'cache_timeout' => '12',
		));
}

register_activation_hook(__FILE__, 'set_om_feed_defaults');
register_activation_hook( __FILE__, 'om_activation' );
/**
 * On activation, set a time, frequency and name of an action hook to be scheduled.
 */
function om_activation()
{
	wp_schedule_event( time(), 'daily', 'om_clean_cache' );
}

add_action( 'om_clean_cache', 'om_flush_cache_daily' );
/**
 * On the scheduled action hook, run the function.
 */
function om_flush_cache_daily()
{
	global $wpdb;

	$sql = "
		delete from {$wpdb->options}
		where option_name like '_transient_caleb_rss_%' or option_name like '_transient_timeout_caleb_rss_%'
		";
	$wpdb->query($sql);
}

register_deactivation_hook( __FILE__, 'om_deactivation' );
/**
 * On deactivation, remove all functions from the scheduled action hook.
 */
function om_deactivation()
{
	wp_clear_scheduled_hook( 'om_clean_cache' );
	delete_option('om-reader_options');
}
?>
