=== OM RSS Feed Reader ===
Contributors: Chris Young <chris.young@om.org>, Leon Hedding <leon.hedding@om.org>, Daniel Fairhead <daniel.fairhead@om.org>
Tags: om, caleb, shortcodes, rssfeed, simple rss, rss reader, twitter
Requires at least: 3.0
Tested up to: 3.8
=======
Stable tag: 0.0.4

== Description ==

Plugin to view an rss feed from Caleb on your page or post. Or add a feed as widget. 

Requires plugins:
- Qtranslate
- Visitor Country

== Installation ==

1. Upload the zip file using the Wordpress installer
   or copy the contents to a folder 'om-feed-reader' in your Plugins folder
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use the shortcode in your posts or pages: 
[om-gc-feed-reader time=11-2006 countryID=UG categoryID=25 text="Surfing" length=10 start=10 families=1/0 groups=1/0 married=1/0 age=16 pastDays=-30 plain=0/1]
or [om-jobs-feed-reader category=1 country=IE region=17 freeText="Programmer" length=10 plain=0/1]
[om-contact-display]
[om-resource] - resource id is pulled from the request string
4. Use the widgets from the widget menu in the Wordpress admin pages

== Changelog ==
= 0.1.8 =
Support for overriding fromCountry in the shortcode tag.
Support for mulitple fromCountries (eg. "GB,FR,DE")
= 0.1.7 =
Layout templates for Jobs, Short-term, Contact and Resources
Sort Short-term and Jobs by Date or Category or Country
= 0.1.6 =
Added buttons to the mce text editor (for short term and jobs)
= 0.1.5 =
Added an admin settings page and added more advanced caching.
= 0.1.4 =
Bugfixes, code cleanup
= 0.1.3 =
Added Jobs widget
= 0.1.2 =
Bugfixes, a new icon
Changed 'Global Challenge' to 'Short Term Missions'
= 0.1.1 =
single resource page handles images as well as articles
= 0.1.0 =
New function: single resource page [om-resource]
= 0.0.4 =
Cleanup of code - removing old wp-simple-rss-feed-reader code.
= 0.0.3 =
Functionality for a second category id (category1 AND category2)
Fixed bug with rss string: changed "categoryid=x" -> "categoryId=x"
= 0.0.2 =
* Added basic caching for the feed reader.
= 0.0.1 =
* The first release
