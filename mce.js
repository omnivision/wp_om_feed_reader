(function() {
    tinymce.create('tinymce.plugins.om_feed_reader', {
        init : function(ed, url) {
	    ed.addButton('shortterm', {
		title : 'Short term missions',
		cmd : 'shortterm',
		image : url + '/images/button-short.png'
	    }); 
	    ed.addButton('jobs', {
		title : 'Job Vacancies', 
		cmd : 'jobs',
		image : url + '/images/button-long.png'
	    }); 
	    ed.addCommand('shortterm', function() {
		var shortcode = '[om-gc-feed-reader';
		var categoryId = prompt("Category ID: (blank for all)");
		var countryId = prompt("Country ID: (blank for all)");
		var text = prompt("Text Search: (blank for none)");
		var startdate = prompt("Start date: (blank for none)");
		var families = prompt("families (1 for yes, 0 for no, blank for both)");
		var groups = prompt("groups (1 for yes, 0 for no, blank for both)");
		var married = prompt("married couples (1 for yes, 0 for no, blank for both)");
		var age = prompt("Age: (blank for all)");
		var pastdays = prompt("Past days: (blank for all)");
		var length = prompt("Number of items: (blank for all)");

		if (categoryId!='') shortcode += ' categoryId="' + categoryId + '"';
		if (countryId!='') shortcode += ' countryId="' + countryId + '"';
		if (text!='') shortcode += ' text="' + text + '"';
		if (startdate!='') shortcode += ' startdate="' + startdate + '"';
		if (families!='') shortcode += ' families="' + families + '"';
		if (groups!='') shortcode += ' groups="' + groups + '"';
		if (married!='') shortcode += ' married="' + married + '"';
		if (age!='') shortcode += ' age="' + age + '"';
		if (pastdays!='') shortcode += ' pastdays="' + pastdays + '"';
		if (length!='') shortcode += ' length="' + length + '"';
		shortcode += ']';
		ed.execCommand('mceInsertContent',0,shortcode);
	    });
	    ed.addCommand('jobs', function() {
		var shortcode = '[om-jobs-feed-reader';
		var categoryId = prompt("Category ID: (blank for all)");
		var countryId = prompt("Country ID: (blank for all)");
		var text = prompt("Text Search:");
		var length = prompt("Number of items: (blank for all)");

		if (categoryId!='') shortcode += ' category="' + categoryId + '"';
		if (countryId!='') shortcode += ' country="' + countryId + '"';
		if (text!='') shortcode += ' freeText="' + text + '"';
		if (length!='') shortcode += ' length="' + length + '"';
		shortcode += ']';
		ed.execCommand('mceInsertContent',0,shortcode);
	    });
        },
 
        createControl : function(n, cm) {
            return null;
        },
 
        getInfo : function() {
            return {
                longname : 'OM Feed Reader Buttons',
                author : 'OMNIvision',
                authorurl : 'http://omnivision.om.org',
                infourl : 'http://webserver.omnivision.om.org/wdemo/',
                version : "0.1"
            };
        }
    });
 
    // Register plugin
    tinymce.PluginManager.add( 'om_feed_reader', tinymce.plugins.om_feed_reader );
})();
