<?php
add_action('init', 'om_feed_reader_buttons');
function om_feed_reader_buttons() {
	add_filter("mce_external_plugins", "om_feed_reader_add_buttons");
	add_filter("mce_buttons", "om_feed_reader_register_buttons");
}
function om_feed_reader_add_buttons($plugin_array) {
	$plugin_array['om_feed_reader'] = plugins_url('mce.js', __FILE__);
	return $plugin_array;
}
function om_feed_reader_register_buttons($buttons) {
	array_push($buttons, 'shortterm', 'jobs'); 
	return $buttons;
}
