<?php
class OMSettingsPage
{
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Start up
	 */
	public function __construct()
	{
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page()
	{
		// This page will be under "Settings"
		add_options_page(
			'OM RSS Feed Reader',
			'OM RSS Feed Reader',
			'manage_options',
			'om-rss-feed-reader',
			array( $this, 'create_admin_page' )
		);

		// display admin notice when cache has been cleared
		add_action( "admin_notices", array ( $this, 'parse_message' ) );
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page()
	{
		// Set class property

		$this->options = get_option( 'om-reader_options' );
		?>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2>OM RSS Feed Reader</h2>
			<form method="post" action="options.php">
			<?php
				// This prints out all hidden setting fields
				settings_fields( 'om_option_group' );
				do_settings_sections( 'om-setting-admin' );
				submit_button("Update Settings");
			?>
			</form>

			<h3>Clear Cache</h3>
			Clear the cache for all feeds.
			<p>
			<form action="admin-post.php" method="post">
				 <?php wp_nonce_field('om-rss-action','om-rss-nonce'); ?>
				<input type="hidden" name="action" value="flush_cache" />
				<input type="submit" name="flush_rss_cache" value="empty cache" class="button" />
			</form>
			</p>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init()
	{
		register_setting(
			'om_option_group', // Option group
			'om-reader_options', // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section(
			'setting_section_id', // ID
			'Feed Reader Settings', // Title
			array( $this, 'print_section_info' ), // Callback
			'om-setting-admin' // Page
		);

		add_settings_field(
			'cache_timeout', // ID
			'RSS Feed Cache Timeout (in hours)', // Title
			array( $this, 'cache_timeout_callback' ), // Callback
			'om-setting-admin', // Page
			'setting_section_id' // Section
		);

		add_settings_field(
			'pin', // ID
			'PIN Number (optional)', // Title
			array( $this, 'pin_callback' ), // Callback
			'om-setting-admin', // Page
			'setting_section_id' // Section
		);

		add_settings_field(
			'field_id',
			'Field ID (optional)',
			array( $this, 'field_id_callback' ),
			'om-setting-admin',
			'setting_section_id'
		);

		add_settings_field(
			'from_country_id',
			'From Country ID (two_letter)',
			array( $this, 'from_country_id_callback' ),
			'om-setting-admin',
			'setting_section_id'
		);

		add_settings_field(
			'show_price',
			'Show price of short term opportunities?',
			array( $this, 'show_price_callback'),
			'om-setting-admin',
			'setting_section_id'
		);

		add_action('admin_post_flush_cache', 'flush_caleb_cache');
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public function sanitize( $input )
	{
		$new_input = array();
		if( isset( $input['cache_timeout'] ) )
			// validate data and always be at least 1 hour.
			$input['cache_timeout'] = absint($input['cache_timeout']);
			$new_input['cache_timeout'] = ($input['cache_timeout'] <> 0 ? $input['cache_timeout'] : 1);

		if( isset( $input['pin'] ) )
			$new_input['pin'] = absint($input['pin']);


		if( isset( $input['field_id'] ) )
			$new_input['field_id'] = sanitize_text_field( $input['field_id'] );

		if( isset( $input['from_country_id'] ) )
			$new_input['from_country_id'] = sanitize_text_field( $input['from_country_id'] );

		if( isset( $input['show_price'] ) )
			$new_input['show_price'] = ($input['show_price'] === 'true');

		return $new_input;
	}

	/**
	 * Print the Section header text
	 */
	public function print_section_info()
	{
		print 'Enter your settings below for this plugin. Not all fields are required and the defaults will work.';
	}

	/**
	 * The following callback functions get the settings
	 * option arrays and print out the values
	 */
	public function cache_timeout_callback()
	{
		printf(
			'<input type="text" id="cache_timeout" name="om-reader_options[cache_timeout]" value="%s" />',
			isset( $this->options['cache_timeout'] ) ? esc_attr( $this->options['cache_timeout']) : ''
		);
	}

	public function pin_callback()
	{
		printf(
			'<input type="text" id="pin" name="om-reader_options[pin]" value="%s" />',
			isset( $this->options['pin'] ) ? esc_attr( $this->options['pin']) : ''
		);
	}

	public function field_id_callback()
	{
		printf(
			'<input type="text" id="Field ID" name="om-reader_options[field_id]" value="%s" />',
			isset( $this->options['field_id'] ) ? esc_attr( $this->options['field_id']) : ''
		);
	}

	public function from_country_id_callback()
	{
		printf(
			'<input type="text" id="From Country ID" name="om-reader_options[from_country_id]" value="%s" />',
			isset( $this->options['from_country_id'] ) ? esc_attr( $this->options['from_country_id']) : ''
		);
	}

	public function show_price_callback()
	{
		printf(
			'<input type="checkbox" name="om-reader_options[show_price]" value="true" %s />',
			(isset($this->options['show_price'])?'checked':''));
	}

	/**
	* Parses the URL field to determine if the cache was cleared and prints to screen if so.
	*/
	public function parse_message()
	{
		if ( ! isset ( $_GET['cache'] ) )
			return;

		if ( 'cleared' === $_GET['cache'] )
			$this->cache_text = 'Cache cleared.';

		echo '<div class="updated"><p>'
			. $this->cache_text . '</p></div>';
	}

}

if( is_admin() )
	$om_settings_page = new OMSettingsPage();


// clear the transient cache
function flush_caleb_cache()
{
	check_admin_referer('om-rss-action', 'om-rss-nonce');

	if ( isset ( $_POST[flush_rss_cache] ) )
	{
		$cache = 'cleared';
	}

	global $wpdb;

	$sql = "
		delete from {$wpdb->options}
		where option_name like '_transient_caleb_rss_%' or option_name like '_transient_timeout_caleb_rss_%'
		";
	$wpdb->query($sql);


	$url = add_query_arg( 'cache', $cache, urldecode( admin_url( 'options-general.php?page=om-rss-feed-reader') ) );
	wp_safe_redirect( $url );
	exit;
}
?>
