<?php extract($item); ?>
<div class="caleb-fdrdr-container">
	<a href="<?php echo $link; ?>" target="_blank" title="<?php echo $title; ?>" class="om-feed-link"><h2><?php echo $title; ?></h2></a>
	<div class="caleb-shortterm" > 
		<img src="<?php echo plugins_url('images/single-s-y.png',__FILE__).'" title="'.__("Single people welcome",'om_feed_reader');?>">
		<img src="<?php echo plugins_url('images/married-s-'.($married == 't' ? 'y':'n').'.png',__FILE__);?>" 
			title="<?php echo (($married == 't')?__("Married couples welcome",'om_feed_reader')
				:__("Sorry, no married couples",'om_feed_reader'));?>">
		<img src="<?php echo plugins_url('images/family-s-'.($family == 't' ? 'y':'n').'.png',__FILE__);?>" 
			title="<?php echo (($family == 't')?__("Families welcome",'om_feed_reader')
				:__("Sorry, no families",'om_feed_reader'));?>">
		<img src="<?php echo plugins_url('images/group-s-'.($group == 't' ? 'y':'n').'.png',__FILE__);?>" 
			title="<?php echo (($group == 't')?__("Groups welcome",'om_feed_reader')
				:__("Sorry, no groups",'om_feed_reader'));?>">

		 | <?php echo "$startDate - $endDate"; ?> |
		  <?php if (isset($global_settings['show_price'])) { echo __("Cost",'om_feed_reader').': '.$cost.' '.$currency; } ?>
	</div>

	<div class="caleb-description" ><p><?php echo $description; ?></p></div>
	<div class="caleb-category" ><?php echo __("Categories",'om_feed_reader').': '.$categories; ?></div>
	<a class="caleb-readon" target="_blank" href="<?php echo $link; ?>"><?php echo __("More info",'om_feed_reader'); ?></a>
</div>
