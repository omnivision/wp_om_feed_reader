<div class="gclist">
<ul class="gcmenu">
<?php foreach ($headings as $item) { ?>
    <li><a href='#<?php echo $item; ?>'><?php echo $item; ?></a></li>
<?php } ?>
</ul>
<div class="gccontent">
<?php foreach ($headings as $heading) { ?>
    <h1><a name='<?php echo $heading; ?>'><?php echo $heading; ?></a></h1>
    <?php foreach ($indexed_items[$heading] as $item) {
            include('item.html.php');
        }
    }?>
</div>
</div>
