<?php
$menu_items = array();
$items = "";
	extract($atts);
	// print_r($options); // WHY?!?!? -DF 2017

	foreach($xml->channel->item as $item) {
		$om = $item->children('om',true);
		$simpleitem = array(
			'title'=>$item->title,
			'link'=>$item->link,
			'description'=>$om->description,
			'categories'=>$om->categories,
			'category'=>$om->category,
			'country'=>$om->country,
			'startDate'=>$om->startDate,
			'endDate'=>$om->endDate,
			'cost'=>$om->cost,
			'currency'=>$om->currency,
			'married'=>$om->marriedWelcome,
			'family'=>$om->familyWelcome,
			'group'=>$om->groupWelcome
		);

		// Narrow the search by a second category
		if (array_key_exists('categoryid2', $atts) &&  $atts['categoryid2']) {
			$feed_categories = explode(',',$om->categoryIds);
			$want_categories = explode(',',$atts['categoryid2']);

			$a_category=false;
			foreach ($want_categories as $want_category) if (in_array($want_category,$feed_categories)) $a_category=true;
			if (!$a_category) continue;
		}

		// TODO based upon shortcode options we need to build a menu by category, date or country
		switch ($groupby) {
			case 'country': default:
				$heading = (String)$om->country;
				$headings[] = $heading;
				$indexed_items[$heading][] = $simpleitem;
				break;
			case 'date':
				$heading = strtotime($om->startDate);
				$headings[] = $heading;
				$indexed_items[$heading][] = $simpleitem;
				break;
			case 'category':
				$categories = explode('|',$om->categories);
				foreach ($categories as $category) {
					$category = trim($category);
					$headings[] = $category;
					$indexed_items[$category][] = $simpleitem;
				}
				break;
		}
	}
	$headings = array_unique($headings);
	asort($headings);
	if ($groupby=='date') {
		foreach($headings as $key=>$heading) $headings[$key] = date('M Y',$heading);
		foreach($indexed_items as $key=>$items)
		foreach ($items as $item) $indexed_items[date('M Y',$key)][] = $item;
		$headings = array_unique($headings);
	}

ob_start();
include_once('templates/default/page.html.php');
$return = ob_get_contents();
ob_end_clean();
$return = (isset($menu)?$menu:'') . $return;
