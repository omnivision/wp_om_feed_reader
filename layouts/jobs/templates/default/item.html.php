<?php extract($item); ?>
<div class="caleb-fdrdr-container">
    <a href="<?php echo $link; ?>" target="_blank" title="<?php echo $title; ?>" class="om-feed-link"><h4><?php echo $title; ?></h4></a>

    <div class="caleb-opportunities "><?php echo __("Start",'om_feed_reader').': '.($startDate);?></div>
    <br />
    <div class="caleb-description" ><span><?php echo $description; ?></span></div>
    <div class="caleb-category" ><?php echo __("Categories",'om_feed_reader').': '.$categories; ?></div>
    <a class="caleb-readon" target="_blank" href="<?php echo $link; ?>"><?php echo __("More info",'om_feed_reader'); ?></a>
    <br /> </p>
</div>
