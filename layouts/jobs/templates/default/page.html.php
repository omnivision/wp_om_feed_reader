<div class="joblist">
<ul class="jobmenu">
<?php foreach ($headings as $item) { ?>
    <li><a href='#<?php echo $item; ?>'><?php echo $item; ?></a></li>
<?php } ?>
</ul>
<div class="jobcontent">
<?php foreach ($headings as $heading) { ?>
    <h3><a name='<?php echo $heading; ?>'><?php echo $heading; ?></a></h3>
    <?php foreach ($indexed_items[$heading] as $item) {
            include('item.html.php');
        }
    }?>
</div>

</div>
